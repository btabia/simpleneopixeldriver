#ifndef WS2812B_H
#define WS2812B_H

#include "mbed.h"

namespace neoPix{
  struct Pixels{
    uint8_t blue;
    uint8_t green;
    uint8_t red;
  };

  class WS2812B{
  public:
    WS2812B(PinName mosi);
    void update(Pixels buffer[], uint32_t length);

  private:

    SPI __spi;
    char SetEightBits(uint8_t byte);
    int  latch_time_us_;

  };
}
#endif
