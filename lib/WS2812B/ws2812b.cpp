#include "ws2812b.h"
using namespace neoPix;

WS2812B::WS2812B(PinName mosi):
__spi(mosi, NC, NC),
latch_time_us_(50)
{
  __spi.frequency(6530608);
  __spi.format(8,1);
}

char WS2812B::SetEightBits(uint8_t byte){
  uint8_t zero = 0b11100000;
  uint8_t one = 0b11111000;
  switch(byte){
    case 0b00000000:
      return zero;
    case 0b00000001:
      return one;
    default:
      return zero;
  }
}


void WS2812B::update(Pixels buffer[], uint32_t length){
  //cs = 1;
  char out[24];
  for(int i = 0; i < length; ++i){
    for(int j = 0; j < 8; ++j){
      out[i + j] = SetEightBits((buffer[i].green >> j) & 0x1);
      out[i + j + 8] = SetEightBits((buffer[i].red >> j) & 0x1);
      out[i + j + 16] = SetEightBits((buffer[i].blue >> j) & 0x1);
    }
    __spi.write(out, 24, NULL, 0);
  }
  //cs = 0;
  wait_us(latch_time_us_);
}
