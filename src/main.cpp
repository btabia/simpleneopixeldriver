#include "mbed.h"
#include "ws2812b.h"
Serial pc(USBTX, USBRX);

void init(void){
  pc.baud(115200);
}

void loop(void){

}

int main() {
   // Create a temporary DigitalIn so we can configure the pull-down resistor.
   // (The mbed API doesn't provide any other way to do this.)
   // An alternative is to connect an external pull-down resistor.


   // The pixel array control class.
   neoPix::WS2812B lamp(D11);

   neoPix::Pixels lights[7];

   for(int i = 0; i < 7; ++i){
     lights[i].green = 0;
     lights[i].red = 0;
     lights[i].blue =0;
   }

   init();

   while (1) {
     // uint16_t val1= SetFourBits((lights[0].blue) & 0xf);
     // uint16_t val2= SetFourBits((lights[0].blue >> 4) & 0xf);
     //
     // pc.printf("val 1: %x val 2: %x\n",val1,val2);
     lamp.update(lights,7);
     // wait(0.7);
   }
 }
